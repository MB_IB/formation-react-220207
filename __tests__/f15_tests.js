import React from 'react'
import ReactDOM from 'react-dom'
import { PiedDePage } from "../src/f15_tests.jsx"

test("Le pied de page est correct", function() {
    const attendu = <p>(c) MB IB 2022</p>
    const obtenu = PiedDePage()
    expect(obtenu).toEqual(attendu)
})