module.exports = {
    "presets": [
        ["@babel/env", {
            "targets": {"browsers":">0.001%"},
            // normal
            "modules": false
            // JEST
            // "modules": "umd"
        }],
        ["@babel/react"]
    ]
}