function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function Compteur() {
  var _React$useState = React.useState({
    "enclos": 0,
    "parcs": 0
  }),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      compte = _React$useState2[0],
      changeCompte = _React$useState2[1];

  var augmenteEnclos = function augmenteEnclos() {
    changeCompte({
      "enclos": compte.enclos + 1,
      "parcs": compte.parcs
    });
  };

  var diminueEnclos = function diminueEnclos() {
    changeCompte({
      "enclos": compte.enclos - 1,
      "parcs": compte.parcs
    });
  };

  var augmenteParcs = function augmenteParcs() {
    changeCompte({
      "enclos": compte.enclos,
      "parcs": compte.parcs + 1
    });
  };

  React.useEffect(function () {
    console.log("Compteur :: mount ou update");
    return function () {
      console.log("Compteur :: unmount");
    };
  });
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("button", {
    onClick: augmenteEnclos
  }, "Un autre enclos"), /*#__PURE__*/React.createElement("button", {
    onClick: diminueEnclos
  }, "Un enclos de moins"), /*#__PURE__*/React.createElement("button", {
    onClick: augmenteParcs
  }, "Un autre parc"), /*#__PURE__*/React.createElement("p", null, compte.enclos, "/11 enclos"), /*#__PURE__*/React.createElement("p", null, compte.parcs, "/8 parcs"));
}

function App() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "J'ai tout vu ?"), /*#__PURE__*/React.createElement("p", null, "Comptez les enclos, en avez-vous rat\xE9s ?"), /*#__PURE__*/React.createElement(Compteur, null));
}

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), document.getElementById("root"));