export function lireNouvelles(changeNews) {
  fetch("f11_ajax.json", {}).then(function (response) {
    return response.json();
  }).then(function (data) {
    console.log("Nouvelles - Données reçues");
    changeNews(data.nouvelles);
  });
}