function Bassins() {
  var bassins = [{
    "id": 1,
    "nom": "Les tortues",
    "volume": 8750
  }, {
    "id": 3,
    "nom": "La mer",
    "volume": 15200
  }, {
    "id": 4,
    "nom": "Le lagon",
    "volume": 2340
  }, {
    "id": 5,
    "nom": "Les profondeurs",
    "volume": 1140
  }]; // liste a puce : <ul><li>...</li><li>...</li><li>...</li></ul>
  // table : <table> <tr><td>..</td><td>..</td></tr> <tr>....</tr> </table>

  var liBassins = [];

  for (var _i = 0, _bassins = bassins; _i < _bassins.length; _i++) {
    var b = _bassins[_i];
    liBassins.push( /*#__PURE__*/React.createElement("li", {
      key: b.id
    }, b.nom, " (de ", b.volume, "m", /*#__PURE__*/React.createElement("sup", null, "3"), ")"));
  }

  return /*#__PURE__*/React.createElement("ul", null, " ", liBassins, " ");
}

function App() {
  /*return <React.Fragment>
      <h1>Dernières nouvelles</h1>               
      <Bassins />
  </React.Fragment>*/
  // ou
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("h1", null, "Derni\xE8res nouvelles"), /*#__PURE__*/React.createElement(Bassins, null));
}

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), document.getElementById("root"));