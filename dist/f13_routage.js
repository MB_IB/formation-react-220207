function PagePoneys() {
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h2", null, "Nos poneys"), /*#__PURE__*/React.createElement("p", null, "Nos poneys...."));
}

function PageMoutons() {
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h2", null, "Nos moutons"), /*#__PURE__*/React.createElement("p", null, "Nos moutons...."));
}

function PageChevres() {
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h2", null, "Nos ch\xEAvres"), /*#__PURE__*/React.createElement("p", null, "Nos ch\xEAvres...."));
}

function App() {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("h1", null, "Ferme p\xE9dagogique"), /*#__PURE__*/React.createElement(ReactRouterDOM.HashRouter, null, /*#__PURE__*/React.createElement(ReactRouterDOM.Link, {
    to: "/poneys"
  }, "Poneys"), " -", /*#__PURE__*/React.createElement(ReactRouterDOM.Link, {
    to: "/moutons"
  }, "Moutons"), " -", /*#__PURE__*/React.createElement(ReactRouterDOM.Link, {
    to: "/chevres"
  }, "Ch\xEAvres"), /*#__PURE__*/React.createElement(ReactRouterDOM.Switch, null, /*#__PURE__*/React.createElement(ReactRouterDOM.Route, {
    path: "/moutons"
  }, /*#__PURE__*/React.createElement(PageMoutons, null)), /*#__PURE__*/React.createElement(ReactRouterDOM.Route, {
    path: "/chevres"
  }, /*#__PURE__*/React.createElement(PageChevres, null)), /*#__PURE__*/React.createElement(ReactRouterDOM.Route, null, /*#__PURE__*/React.createElement(PagePoneys, null)))));
}

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), document.getElementById("root"));