var titreStr = "Mon Zoo"; // afficher soit "Bonjour, bienvenue sur....", soit "Bonsoir, bienvenue..."

var heure = new Date().getHours(); // renvoie de 0 à 23

var message = "Bonsoir";
if (heure < 18) message = "Bonjour";
var titre = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, titreStr + "!"), /*#__PURE__*/React.createElement("p", null, message, ", bienvenue sur notre site"));
ReactDOM.render(titre, document.getElementById("root"));