function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

/*
* props.titre (obligatoire) titre de la section
* props.soustitre (optionnel) sous-titre de la section
*/
function Section(props) {
  if (props.titre == undefined) props.titre = "Titre par défaut";
  var st = undefined;
  if (props.soustitre != undefined) st = /*#__PURE__*/React.createElement("h3", null, props.soustitre);
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h2", null, props.titre), st, props.children);
}

var Lion = /*#__PURE__*/function (_React$Component) {
  _inherits(Lion, _React$Component);

  var _super = _createSuper(Lion);

  function Lion(props) {
    _classCallCheck(this, Lion);

    return _super.call(this, props);
  }

  _createClass(Lion, [{
    key: "render",
    value: function render() {
      var sexe = "♂";
      if (this.props.femelle) sexe = "♀";
      var style = {
        "color": this.props.couleur,
        // NON "font-size":
        "fontSize": "8pt"
      };
      return /*#__PURE__*/React.createElement("p", {
        style: style
      }, this.props.nom, ", ", this.props.age, " ans, ", sexe);
    }
  }]);

  return Lion;
}(React.Component);

function App() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "Nos lions"), /*#__PURE__*/React.createElement(Section, {
    titre: "Nourriture",
    soustitre: "Dans notre zoo"
  }, "Chacun de nos lions se nourrit de ", /*#__PURE__*/React.createElement("span", {
    className: "numero"
  }, "25"), " \xE0", /*#__PURE__*/React.createElement("span", {
    className: "numero"
  }, "45"), "kg de viande tous les 4 jours"), /*#__PURE__*/React.createElement(Section, {
    titre: "Nos adultes"
  }, /*#__PURE__*/React.createElement(Lion, {
    nom: "Ann",
    age: 8,
    femelle: true,
    couleur: "red"
  }), /*#__PURE__*/React.createElement(Lion, {
    nom: "Bob",
    age: 6,
    femelle: false,
    couleur: "orange"
  })), /*#__PURE__*/React.createElement(Section, {
    titre: "Les lionceaux"
  }, /*#__PURE__*/React.createElement(Lion, {
    nom: "Carla",
    age: 1,
    femelle: true
  }), /*#__PURE__*/React.createElement(Lion, {
    nom: "Dan",
    age: 2
  })));
}

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), document.getElementById("root"));