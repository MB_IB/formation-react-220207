function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var ZoneTexte = /*#__PURE__*/function (_React$Component) {
  _inherits(ZoneTexte, _React$Component);

  var _super = _createSuper(ZoneTexte);

  function ZoneTexte(props) {
    var _this;

    _classCallCheck(this, ZoneTexte);

    _this = _super.call(this, props);
    _this.state = {
      "valeur": ""
    };
    return _this;
  }

  _createClass(ZoneTexte, [{
    key: "texteChange",
    value: function texteChange(event) {
      this.setState({
        "valeur": event.target.value
      });
    }
  }, {
    key: "lire",
    value: function lire() {
      return this.state.valeur;
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/React.createElement("p", null, /*#__PURE__*/React.createElement("label", null, this.props.label, " : ", /*#__PURE__*/React.createElement("input", {
        type: "text",
        onChange: function onChange(event) {
          return _this2.texteChange(event);
        }
      })));
    }
  }]);

  return ZoneTexte;
}(React.Component);

var ZoneSelection = /*#__PURE__*/function (_ZoneTexte) {
  _inherits(ZoneSelection, _ZoneTexte);

  var _super2 = _createSuper(ZoneSelection);

  function ZoneSelection() {
    _classCallCheck(this, ZoneSelection);

    return _super2.apply(this, arguments);
  }

  _createClass(ZoneSelection, [{
    key: "render",
    value:
    /*constructor(props) {
        super(props)
        this.state.valeur2 ="....."
    }*/
    function render() {
      var _this3 = this;

      return /*#__PURE__*/React.createElement("p", null, /*#__PURE__*/React.createElement("label", null, this.props.label, " : ", /*#__PURE__*/React.createElement("select", {
        onChange: function onChange(event) {
          return _this3.texteChange(event);
        }
      }, /*#__PURE__*/React.createElement("option", null, this.props.options[0]), /*#__PURE__*/React.createElement("option", null, this.props.options[1]), /*#__PURE__*/React.createElement("option", null, this.props.options[2]))));
    }
  }]);

  return ZoneSelection;
}(ZoneTexte);

var FormulaireHotel = /*#__PURE__*/function (_React$Component2) {
  _inherits(FormulaireHotel, _React$Component2);

  var _super3 = _createSuper(FormulaireHotel);

  function FormulaireHotel(props) {
    var _this4;

    _classCallCheck(this, FormulaireHotel);

    _this4 = _super3.call(this, props);
    _this4.state = {
      "message": ""
    };
    return _this4;
  }

  _createClass(FormulaireHotel, [{
    key: "envoi",
    value: function envoi() {
      this.setState({
        "message": "Merci, " + this.prenom.lire() + " " + this.nom.lire() + " pour votre demande"
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      return /*#__PURE__*/React.createElement("form", null, /*#__PURE__*/React.createElement(ZoneTexte, {
        label: "Votre nom",
        ref: function ref(zt) {
          _this5.nom = zt;
        }
      }), /*#__PURE__*/React.createElement(ZoneTexte, {
        label: "Votre pr\xE9nom",
        ref: function ref(zt) {
          _this5.prenom = zt;
        }
      }), /*#__PURE__*/React.createElement(ZoneTexte, {
        label: "Votre email",
        ref: function ref(zt) {
          _this5.email = zt;
        }
      }), /*#__PURE__*/React.createElement(ZoneTexte, {
        label: "Votre tel",
        ref: function ref(zt) {
          _this5.tel = zt;
        }
      }), /*#__PURE__*/React.createElement(ZoneSelection, {
        label: "Logement souhait\xE9",
        ref: function ref(zt) {
          _this5.logement = zt;
        },
        options: ["Volière", "Elephants", "Tigres"]
      }), /*#__PURE__*/React.createElement("p", null, /*#__PURE__*/React.createElement("input", {
        type: "button",
        value: "Demander",
        onClick: function onClick() {
          return _this5.envoi();
        }
      })), /*#__PURE__*/React.createElement("p", null, this.state.message));
    }
  }]);

  return FormulaireHotel;
}(React.Component);

function App() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "Hotel"), /*#__PURE__*/React.createElement(FormulaireHotel, null));
}

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), document.getElementById("root"));