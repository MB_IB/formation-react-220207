function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var ContexteReconnaissance = React.createContext({});

var Option = /*#__PURE__*/function (_React$Component) {
  _inherits(Option, _React$Component);

  var _super = _createSuper(Option);

  function Option(props) {
    var _this;

    _classCallCheck(this, Option);

    _this = _super.call(this, props);
    _this.state = {
      "coche": false
    };
    return _this;
  }

  _createClass(Option, [{
    key: "clic",
    value: function clic(event) {
      this.setState({
        "coche": event.target.checked
      });
      this.context.f(this.props.cle, event.target.checked);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/React.createElement("p", null, /*#__PURE__*/React.createElement("label", null, /*#__PURE__*/React.createElement("input", {
        type: "checkbox",
        onChange: function onChange(event) {
          _this2.clic(event);
        }
      }), this.props.children));
    }
  }]);

  return Option;
}(React.Component);

Option.contextType = ContexteReconnaissance;

var Resultat = /*#__PURE__*/function (_React$Component2) {
  _inherits(Resultat, _React$Component2);

  var _super2 = _createSuper(Resultat);

  function Resultat() {
    _classCallCheck(this, Resultat);

    return _super2.apply(this, arguments);
  }

  _createClass(Resultat, [{
    key: "render",
    value: function render() {
      if (this.context.etat.pattes) {
        if (this.context.etat.fourrure) return /*#__PURE__*/React.createElement("p", null, "C'est un ours");else return /*#__PURE__*/React.createElement("p", null, "C'est un chacal");
      } else if (this.context.etat.bec) {
        if (this.context.etat.crete) return /*#__PURE__*/React.createElement("p", null, "C'est un coq");else return /*#__PURE__*/React.createElement("p", null, "C'est une autruche");
      } else return /*#__PURE__*/React.createElement("p", null, "C'est un serpent");
    }
  }]);

  return Resultat;
}(React.Component);

Resultat.contextType = ContexteReconnaissance;

var Reconnaissance = /*#__PURE__*/function (_React$Component3) {
  _inherits(Reconnaissance, _React$Component3);

  var _super3 = _createSuper(Reconnaissance);

  function Reconnaissance(props) {
    var _this3;

    _classCallCheck(this, Reconnaissance);

    _this3 = _super3.call(this, props);
    _this3.state = {
      "pattes": false,
      "bec": false,
      "fourrure": false,
      "crete": false
    };
    return _this3;
  }

  _createClass(Reconnaissance, [{
    key: "changeOption",
    value: function changeOption(cle, valeur) {
      var options = {};
      options[cle] = valeur;
      this.setState(options);
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return /*#__PURE__*/React.createElement(ContexteReconnaissance.Provider, {
        value: {
          "etat": this.state,
          "f": function f(c, v) {
            _this4.changeOption(c, v);
          }
        }
      }, /*#__PURE__*/React.createElement(Option, {
        cle: "pattes"
      }, "Il a 4 pattes"), /*#__PURE__*/React.createElement(Option, {
        cle: "bec"
      }, "Il a un bec"), /*#__PURE__*/React.createElement(Option, {
        cle: "fourrure"
      }, "Il a de la fourrure"), /*#__PURE__*/React.createElement(Option, {
        cle: "crete"
      }, "Il a une cr\xEAte"), /*#__PURE__*/React.createElement(Resultat, null));
    }
  }]);

  return Reconnaissance;
}(React.Component);

function App() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "C'\xE9tait quoi ?"), /*#__PURE__*/React.createElement("p", null, "Reconnaissez l'un de nos animaux"), /*#__PURE__*/React.createElement(Reconnaissance, null));
}

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), document.getElementById("root"));