function Compteur() {
    const [compte, changeCompte] = React.useState( {"enclos":0, "parcs":0} )
    const augmenteEnclos = ()=>{
        changeCompte({"enclos": compte.enclos+1, "parcs":compte.parcs})
    }
    const diminueEnclos = ()=>{
        changeCompte({"enclos": compte.enclos-1, "parcs":compte.parcs})
    }
    const augmenteParcs = ()=>{
        changeCompte({"enclos": compte.enclos, "parcs": compte.parcs+1})
    }
    React.useEffect(
        () => { 
            console.log("Compteur :: mount ou update") 
            return () => { console.log("Compteur :: unmount")  }
        }
    )
    return <section>
        <button onClick={ augmenteEnclos }>Un autre enclos</button>
        <button onClick={ diminueEnclos }>Un enclos de moins</button>
        <button onClick={ augmenteParcs }>Un autre parc</button>
        <p>{compte.enclos}/11 enclos</p>
        <p>{compte.parcs}/8 parcs</p>
    </section>
}

function App() {
    return <div>
        <h1>J'ai tout vu ?</h1>       
        <p>Comptez les enclos, en avez-vous ratés ?</p>
        <Compteur />
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)