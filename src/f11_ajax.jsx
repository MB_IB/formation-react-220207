import { lireNouvelles } from "./f11_module.js"

function Nouvelles() {
    const [news, changeNews] = React.useState( [] )
    function demander() {
        lireNouvelles(changeNews)
    }
    let liNews = []
    for(const i in news)
        liNews.push( <li key={i}>{news[i]}</li> )    
    return <section>
        <button onClick={demander}>Afficher</button>
        <ul>{liNews}</ul>
    </section>
}

function App() {
    return <div>
        <h1>Dernières nouvelles</h1>               
        <Nouvelles />
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)