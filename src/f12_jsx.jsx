
function Bassins() {
    const bassins = [
        {"id":1, "nom":"Les tortues", "volume": 8750},
        {"id":3, "nom":"La mer", "volume": 15200},
        {"id":4, "nom":"Le lagon", "volume": 2340},
        {"id":5, "nom":"Les profondeurs", "volume": 1140}
    ]
    // liste a puce : <ul><li>...</li><li>...</li><li>...</li></ul>
    // table : <table> <tr><td>..</td><td>..</td></tr> <tr>....</tr> </table>
    let liBassins = []
    for(const b of bassins) {
        liBassins.push( <li key={b.id}>{b.nom} (de {b.volume}m<sup>3</sup>)</li> )
    }
    return <ul> {liBassins} </ul>
}

function App() {
    /*return <React.Fragment>
        <h1>Dernières nouvelles</h1>               
        <Bassins />
    </React.Fragment>*/
    // ou
    return <>
        <h1>Dernières nouvelles</h1>               
        <Bassins />
    </>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)