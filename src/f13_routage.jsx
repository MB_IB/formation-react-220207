function PagePoneys() {
    return <section>
        <h2>Nos poneys</h2>
        <p>Nos poneys....</p>
    </section>
}

function PageMoutons() {
    return <section>
        <h2>Nos moutons</h2>
        <p>Nos moutons....</p>
    </section>
}

function PageChevres() {
    return <section>
        <h2>Nos chêvres</h2>
        <p>Nos chêvres....</p>
    </section>
}

function App() {
    return <>
        <h1>Ferme pédagogique</h1>
        <ReactRouterDOM.HashRouter>
            <ReactRouterDOM.Link to="/poneys">Poneys</ReactRouterDOM.Link> -
            <ReactRouterDOM.Link to="/moutons">Moutons</ReactRouterDOM.Link> -
            <ReactRouterDOM.Link to="/chevres">Chêvres</ReactRouterDOM.Link>
            <ReactRouterDOM.Switch>
                <ReactRouterDOM.Route path="/moutons">
                    <PageMoutons />
                </ReactRouterDOM.Route>
                <ReactRouterDOM.Route path="/chevres">
                    <PageChevres />
                </ReactRouterDOM.Route>
                <ReactRouterDOM.Route>
                    <PagePoneys />
                </ReactRouterDOM.Route>
            </ReactRouterDOM.Switch>
        </ReactRouterDOM.HashRouter>
    </>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)