function DescriptionAvecErreur() {
    return FonctionQuiNexistePas()
}

function FinAvecErreur() {
    function clic() {
        FonctionQuiNexistePas()
    }
    return <p>.. et c'est pour cela qu'il y fait froid
        <button onClick={clic}>Fin</button>
    </p>
}

class Essayer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {"erreur":null}
    }
    componentDidCatch(err, info) {
        console.log("Erreur : "+err.toString())
        console.log(info)
        this.setState( {"erreur":err} )
    }
    render() {
        if(this.state.erreur!=null) 
            return <p>Erreur ! {this.state.erreur.toString()}</p>
        else
            return this.props.children
    }
}

function App() {
    return <>
        <h1>La banquise</h1>
        <Essayer><DescriptionAvecErreur /></Essayer>
        <Essayer><FinAvecErreur /></Essayer>
    </>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)