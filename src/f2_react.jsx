const titreStr = "Mon Zoo"
// afficher soit "Bonjour, bienvenue sur....", soit "Bonsoir, bienvenue..."
const heure = new Date().getHours() // renvoie de 0 à 23
let message = "Bonsoir"
if(heure<18)
    message = "Bonjour"
let titre = <div>
    {<h1>{titreStr + "!"}</h1>}
    <p>{message}, bienvenue sur notre site</p>
</div>

ReactDOM.render(
    titre,
    document.getElementById("root")
)