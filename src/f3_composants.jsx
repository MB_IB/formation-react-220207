function Titre() {
    return <h1>Les amphibiens</h1>
}

function Intro() {
    return <p>Les amphibiens de notre zoo sont particulièrement 
        dépendant des saisons de notre climat...
    </p>
}

function InfoHiver() {
    return <p>Retrouvez dans notre vivarium les tortues, serpents...</p>
}
function InfoEte() {
    return <p>Retrouvez en plein air les varans, lézards...</p>
}

function InformationSaisonniere() {
    const mois = new Date().getMonth()
    if(mois>=3 && mois<=9) // avril à octobre
        return <InfoEte />
    else
        return <InfoHiver />
}

class Avertissement extends React.Component {
    constructor(p) {
        console.log("Avertissement :: ctor")
        super(p) // appel du constructeur de la classe mère
        this.state = { "message" : "Attention, ça mord !"}
    }
    render() {
        console.log("Avertissement :: render")
        return <p>{this.state.message}</p>
    }
    componentDidMount() {
        console.log("Avertissement :: did mount")
    }
    componentDidUpdate() {
        console.log("Avertissement :: did update")
    }
    componentWillUnmount() {
        console.log("Avertissement :: will unmount")
    }
}

function App() {
    return <div>
        <Titre />
        <Intro />
        <InformationSaisonniere />
        <Avertissement />
        </div>
}

ReactDOM.render(
    // App(),
    // OU <App></App>, OU 
    <App />,
    document.getElementById("root")
)