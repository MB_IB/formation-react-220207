/*
* setTimer()  / clearTimer() : une seule fois
* setInterval() / clearInterval() : plusieurs fois
*/
class ProchainSpectacle extends React.Component {
    constructor(p) {
        super(p)
        this.state = {
            "maintenant" : new Date(),
            "spectacle" : new Date(2022, 1, 8, 17, 0)
        }
    }
    componentDidMount() {
        // Appelle la méthode "uneSeconde" une fois toutes les 1000 ms
        this.chrono = setInterval( () => {this.uneSeconde()} , 1000)
    }
    componentWillUnmount() {
        clearInterval(this.chrono)
    }
    uneSeconde() {
        // NON : this.state.maintenant = new Date()
        this.setState({"maintenant" : new Date()}) // appel à render()
    }
    render() {
        // ms : temps restantn en millisecondes
        let ms = this.state.spectacle.getTime() - 
            this.state.maintenant.getTime()
        let h = parseInt(ms / 3600000)
        let m = parseInt(ms / 60000) % 60
        let s = parseInt(ms / 1000) % 60
        // exemple : "Prochain spectacle dans 7:18:23"
        return <p>Prochain spectacle dans {h}:{m}:{s}</p>
    }
}


function App() {
    return <div>
        <h1>La volière</h1>
        <ProchainSpectacle />
        </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)