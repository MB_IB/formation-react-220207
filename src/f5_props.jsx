/*
* props.titre (obligatoire) titre de la section
* props.soustitre (optionnel) sous-titre de la section
*/
function Section(props) {
    if(props.titre == undefined) 
        props.titre = "Titre par défaut"
    let st = undefined
    if(props.soustitre != undefined)
        st = <h3>{props.soustitre}</h3>
    return <div>
        <h2>{props.titre}</h2>
        {st}
        {props.children}
    </div>
}

class Lion extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let sexe = "♂"
        if(this.props.femelle)
            sexe = "♀"
        let style = {
            "color":this.props.couleur,
            // NON "font-size":
            "fontSize":"8pt"
        }
        return <p style={style}>
            {this.props.nom}, {this.props.age} ans, {sexe}
            </p>
    }
}

function App() {
    return <div>
        <h1>Nos lions</h1>       
        <Section titre="Nourriture" soustitre="Dans notre zoo">
            Chacun de nos lions se nourrit
            de <span className="numero">25</span> à 
            <span className="numero">45</span>kg 
            de viande tous les 4 jours</Section>
        <Section titre="Nos adultes">
            <Lion nom="Ann" age={8} femelle={true} couleur="red" />
            <Lion nom="Bob" age={6} femelle={false} couleur="orange" />
        </Section>
        <Section titre="Les lionceaux">
            <Lion nom="Carla" age={1} femelle />
            <Lion nom="Dan" age={2} />
        </Section>
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)