
class Bloc extends React.Component {
    constructor(props) {
        super(props)
        // redondant avec l'utilisation de la => à la ligne 13
        this.h2click = this.h2click.bind(this)
        this.state = {"afficher":false}
    }
    h2click() {
        console.log("click sur h2 - "+this.props.titre)
        // A EVITER : this.setState({"afficher": !this.state.afficher })
        this.setState(
            // OK (ancienEtat)=>{ return {"afficher": !ancienEtat.afficher } }
            function(ancienEtat){ return {"afficher": !ancienEtat.afficher } }
        )
    }
    render() {
        let detail = undefined
        if(this.state.afficher) {
            detail = <p>{this.props.children}</p>
        }
        return <div>
            <h2 onClick={ ()=>this.h2click() }>{this.props.titre}</h2>
            {detail}
        </div>
    }
}

function App() {
    return <div>
        <h1>Nos léopards</h1>       
        <Bloc titre="Leur habitat">Le léopard est habituellement 
        rencontré en Afrique et en Asie du sud-est...</Bloc>
        <Bloc titre="Nos animaux">Depuis quelques années, notre
        zoo accueille Eliza et Frank, un couple de léopards ethiopiens
        reccueillis après leur fuite de leur propriétaire privé...</Bloc>
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)