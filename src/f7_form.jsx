class FormulaireTarifs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "adultes" : 1, 
            "enfants" : 0, 
            "message" : ""
        }
    }
    adultesChange(event) {
        this.setState( {"adultes" : event.target.value } )
    }
    enfantsChange(event) {
        this.setState( {"enfants" : event.target.value } )
    }
    envoi() {
        // 13e par adulte, 6.5 par enfants
        const total = this.state.adultes * 13 + this.state.enfants * 6.5
        this.setState({"message":"Votre entrée pour seulement : "+total+"€"})
    }
    render() {
        return <form>
            <p><label>Adultes : <input type="number" min={1} value={this.state.adultes}
                    onChange={ (event)=>this.adultesChange(event) } /></label></p>
            <p><label>Enfants : <input type="number" min={0} value={this.state.enfants} 
                    onChange={ (event)=>this.enfantsChange(event) } /></label></p>
            <p><input type="button" value="Calculer" 
                    onClick={ ()=>this.envoi() } /></p>
            <p>{this.state.message}</p>
        </form>
    }
}

function App() {
    return <div>
        <h1>Tarifs</h1>       
        <FormulaireTarifs />
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)