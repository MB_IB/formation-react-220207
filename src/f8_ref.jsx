class ZoneTexte extends React.Component {
    constructor(props) {
        super(props)
        this.state = {"valeur":""}
    }
    texteChange(event) {
        this.setState({"valeur": event.target.value})
    }
    lire() {
        return this.state.valeur
    }
    render() {
        return <p><label>{this.props.label} : <input type="text" 
            onChange={ (event)=>this.texteChange(event) } /></label></p>
    }
}

class ZoneSelection extends ZoneTexte {
    /*constructor(props) {
        super(props)
        this.state.valeur2 ="....."
    }*/
    render() {
        return <p><label>{this.props.label} : <select
                    onChange={ (event)=>this.texteChange(event) } >
                <option>{this.props.options[0]}</option>
                <option>{this.props.options[1]}</option>
                <option>{this.props.options[2]}</option>
            </select></label></p>
    }
}

class FormulaireHotel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {"message":""}
    }    
    envoi() {
        this.setState({"message":"Merci, "+this.prenom.lire()+" "+
            this.nom.lire()+" pour votre demande"})
    }
    render() {
        return <form>
            <ZoneTexte label="Votre nom" ref={ (zt)=>{ this.nom=zt } } />
            <ZoneTexte label="Votre prénom" ref={ (zt)=>{ this.prenom=zt } } />
            <ZoneTexte label="Votre email" ref={ (zt)=>{ this.email=zt } } />
            <ZoneTexte label="Votre tel" ref={ (zt)=>{ this.tel=zt } } />
            <ZoneSelection label="Logement souhaité" 
                ref={ (zt)=>{ this.logement=zt } }
                options={ ["Volière", "Elephants", "Tigres"] } />

            <p><input type="button" value="Demander"
                onClick={ ()=>this.envoi() } /></p>
            <p>{this.state.message}</p>
        </form>
    }
}

function App() {
    return <div>
        <h1>Hotel</h1>       
        <FormulaireHotel />
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)