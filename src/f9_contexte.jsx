const ContexteReconnaissance = React.createContext( {} )

class Option extends React.Component {
    constructor(props) {
        super(props)
        this.state = {"coche":false}
    }
    clic(event) {
        this.setState({ "coche":event.target.checked })
        this.context.f(this.props.cle, event.target.checked)
    }
    render() {
        return <p><label>
            <input type="checkbox" onChange={ (event)=>{ this.clic(event) } } />
            {this.props.children}</label></p>
    }
}
Option.contextType = ContexteReconnaissance

class Resultat extends React.Component {
    render() {
        if(this.context.etat.pattes) 
            if(this.context.etat.fourrure) 
                return <p>C'est un ours</p>
            else
                return <p>C'est un chacal</p>
        else
            if(this.context.etat.bec) 
                if(this.context.etat.crete) 
                    return <p>C'est un coq</p>
                else
                    return <p>C'est une autruche</p>
            else
                return <p>C'est un serpent</p>
    }
}
Resultat.contextType = ContexteReconnaissance

class Reconnaissance extends React.Component {
    constructor(props) {
        super(props)
        this.state = {"pattes": false, "bec":false, "fourrure":false, "crete":false}
    }
    changeOption(cle, valeur) {
        let options = {}
        options[cle] = valeur
        this.setState(options)
    }
    render() {
        return <ContexteReconnaissance.Provider value={ 
                {"etat":this.state, "f":(c,v)=>{ this.changeOption(c,v) } } }>
            <Option cle="pattes">Il a 4 pattes</Option>
            <Option cle="bec">Il a un bec</Option>
            <Option cle="fourrure">Il a de la fourrure</Option>
            <Option cle="crete">Il a une crête</Option>
            <Resultat />
        </ContexteReconnaissance.Provider>
    }
}

function App() {
    return <div>
        <h1>C'était quoi ?</h1>       
        <p>Reconnaissez l'un de nos animaux</p>
        <Reconnaissance />
    </div>
}

ReactDOM.render(
    <App />,
    document.getElementById("root")
)